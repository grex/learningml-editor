import { Label, TText, ITrainResult, IProject, ILabeledImage, ILabeledText, ILabeledData, MLState } from 'src/app/interfaces/interfaces';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import { ClassifierService } from '../../services/classifier.service';
import { ShowProgressSpinnerService } from '../../services/show-progress-spinner.service';
import { ScratchManagerService } from '../../services/scratch-manager.service';
import { LabeledDataManagerService } from '../../services/labeled-data-manager.service';
import { slideInAnimation } from 'src/app/animations';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ImageClassifierService } from 'src/app/services/image-classifier.service';
import { ConfigService } from 'src/app/services/config.service';

type DialogData = Label;

@Component({
  selector: 'app-ml-modelo',
  templateUrl: './ml-model.component.html',
  styleUrls: ['./ml-model.component.css'],
  animations: [slideInAnimation]
})
export class MlModelComponent implements OnInit {

  panelOpenState = false;
  labels = new Set<Label>();
  trainResult: ITrainResult;
  isLinear = true;
  activeLang = 'es';
  cameraStatus = false;
  loading = false;
  language = 'gl';

  constructor(
    private route: ActivatedRoute,
    private translate: TranslateService,
    private authService: AuthenticationService,
    public labeledDataManager: LabeledDataManagerService,
    public classifierService: ClassifierService,
    public imageClassifierService: ImageClassifierService,
    private scratchManager: ScratchManagerService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog,
    private progressSpinner: ShowProgressSpinnerService) {
      this.language = localStorage.getItem('language');
  }

  ngOnInit() {

    let ch = new BroadcastChannel('model-channel');
    ch.addEventListener('message', e => {
      console.log("he recibido este dato");
      console.log(e.data);
      if (e.data.image) {
        let img = new Image();
        img.src = e.data.data;
        img.onload = () => {
          this.labeledDataManager.addEntry(
            {
              label: e.data.label,
              data: img
            }
          );
        }
      } else {
        this.labeledDataManager.addEntry(e.data);
      }

    });

    //this.translate.setDefaultLang(this.activeLang);
    if (localStorage.getItem("language") == null) {
      this.translate.setDefaultLang("es");
    } else {
      this.translate.setDefaultLang(localStorage.getItem("language"))
    }

    this.route.params.subscribe(params => {
      this.labeledDataManager.modelType = params['type'];

      this.route.queryParams.subscribe(
        (v: IProject) => {
          // console.log(v);
          if (v.id != undefined) {
            this.loadFromServer(v.id);
          }
        }
      );
    });

    window.addEventListener("storage", data => {

      console.log(data);
      let model = localStorage.getItem("easyml_model");
      let modelObj = JSON.parse(model);
      this.classifierService.fromJSON(modelObj.modelJSON);

    }, false);
  }

  loadFromServer(id: number) {

    let keys_to_translate = [
      'model.init_sesion.title',
      'model.init_sesion.message',
    ]

    this.translate.get('model.init_sesion').subscribe((res: string) => {
      console.log(res);

    });

    if (!this.authService.isAuthenticated) {
      this.snackBar.open("tienes que iniciar sesión antes para poder guardar el proyecto en el servidor",
        'Inicia sesión antes', {
        duration: 3000,
        verticalPosition: 'top'
      });
    }
    this.loading = true;
    this.labeledDataManager.loadFromServer(id).subscribe(
      v => {
        if (this.labeledDataManager.modelType == "text") {
          this.classifierService.clear("es");
          // Esto tan lioso es un cast de <Map<string, Set<ExampleData>> a <Map<string, Set<TText>>
          //this.classifierService.setTraindata(<Map<string, Set<TText>>>this.labeledDataManager.labelsWithData);
        }
        if (this.labeledDataManager.modelType == "image") {
          //this.imageClassifierService.clear();
          //this.imageClassifierService.setTraindata(<Map<string, Set<HTMLImageElement>>>this.labeledDataManager.labelsWithData)
        }
        this.loading = false;

      },
      e => {
        this.loading = false;
        // console.log(e);
        let message = "Ha ocurrido un problema con el servidor, no se ha podido cargar el proyecto";
        if (e.status == 401) {
          message = "Se requiere iniciar sesión para cargar proyectos desde el servidor";
        } else if (e.status >= 402 && e.status < 500) {
          message = "Ese proyecto no existe";
        }
        // console.log(e.status);
        this.snackBar.open(message,
          '¡Ooohhhh!', {
          duration: 3000,
          verticalPosition: 'top'
        });

      }
    )
  }

  public changeLanguage(lang) {
    this.activeLang = lang;
    this.translate.use(lang);
  }


  getConfig() {
    return this.classifierService.getConfiguration();
  }


  saveInputLabeledTexts() {
    this.labeledDataManager.save();
  }


  train() {

    // por lo pronto no tengo adaptado el NPL al gallego ni al catalán ni al italiano ni alemán, así que lo hago en español
    let lang = (this.language == 'gl' || this.language == 'ca' || this.language == 'it' || this.language == 'de' || this.language == 'Ελληνικά')? 'es' : this.language;

    let keys_to_translate = [
      'model.train_no_data.title',
      'model.train_no_data.message',
      'model.train_new_data.title',
      'model.train_new_data.message',
      'model.learning_from_data',
      'model.learned',
      'model.ready_to_be_used'
    ]

    this.translate.get(keys_to_translate).subscribe((res: string) => {
      console.log(res);

      if (this.labeledDataManager.state == MLState.EMPTY) {
        this.snackBar.open(res['model.train_no_data.message'],
          res['model.train_no_data.title'], {
          duration: 3000,
        });
        return;
      } else if (this.labeledDataManager.state == MLState.TRAINED) {
        this.snackBar.open(res['model.train_new_data.message'],
          res['model.train_new_data.title'], {
          duration: 3000,
        });
        return;
      }

      // console.log("aqui");
      let trainObservable = this.labeledDataManager.modelType == 'text' ?
        this.classifierService.train(lang) :
        this.imageClassifierService.train();

      let d = this.progressSpinner
        .showProgressSpinnerUntilExecuted(trainObservable,
          res['model.learning_from_data'], "assets/images/modern-times.gif",
          res['model.learned'], res['model.ready_to_be_used']);

    });

  }

  addLabel() {

    let keys_to_translate = [
      'model.label_created.title',
      'model.label_created.message',
    ]

    this.translate.get(keys_to_translate).subscribe((res: string) => {
      const dialogRef = this.dialog.open(MlAddLabelDialogComponent, {
        width: '250px',
        data: '',
      });

      dialogRef.afterClosed().subscribe(label => {
        // console.log('The dialog was closed');
        if (label == "" || label == undefined) return;
        this.labeledDataManager.addLabel(label);
        this.snackBar.open( res['model.label_created.message'] + '`' + label + '`',
          res['model.label_created.title'], {
          duration: 2000,
        });
      });
    })
  }

  onDeleted($event) {
    this.labels.delete($event);
    this.labeledDataManager.removeLabel($event);
  }

  webcamTakeSnapshot(label) {
    // console.log(label);
  }

  loadScratch() {
    this.scratchManager.load();
  }

}


@Component({
  templateUrl: 'ml-add-label-dialog.html',
})
export class MlAddLabelDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<MlAddLabelDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {

  }

  close(event) {
    // console.log(event);
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
