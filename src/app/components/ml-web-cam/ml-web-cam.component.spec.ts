import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MlWebCamComponent } from './ml-web-cam.component';

describe('MlWebCamComponent', () => {
  let component: MlWebCamComponent;
  let fixture: ComponentFixture<MlWebCamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MlWebCamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MlWebCamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
